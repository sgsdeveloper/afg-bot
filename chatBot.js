var s_id = 'None';
var context = 'default';
var clicke = ''
var sessionid='';
function getSessionId() {
	$.ajax({
		url: '/session',
		type: 'GET',
		async : false,
		success: function (data, textStatus, xhr) {			
			sessionid=data;     
			localStorage.setItem("sessionId", sessionid);       
		},
		error: function (xhr, textStatus, errorThrown) {
			console.log('Error in Operation');		
		}
	}); 
}

getSessionId();

function chatEntered() {
	  var keyCode = event.charCode || event.keyCode;
	  var text = String.fromCharCode(keyCode);	
	  if (keyCode === 13) {
		 
		  var text = document.getElementById("chat-input").value;	
		 if (text == "") {
			  event.preventDefault();
			  return false;
		  } else {
				  document.getElementById("chat-input").blur();
			  setUserResponse(text);
			  send(text);
			  event.preventDefault();
			  return false;
		  }
		  
	  }
}
  
	  //------------------------------------------- Call  D FLOW API--------------------------------------
function send(text) {
  $.ajax({
	  url: '/dflow',
	  type: 'GET',
	  data:{
 		  "minput": text,
		  "s_id":sessionid,
		  "context":context,
		  "event":clicke
	  },
            
	  dataType:"json",
	  contentType:"application/json",
	  async : false,
	  success: function (data, textStatus, xhr) {
	//	alert("Success reply : "+data.message+" sid: "+data.s_id+" context: "+data.context); 
		debugger;               
	    setBotResponse(data);
	  },
	  error: function (xhr, textStatus, errorThrown) {
	//	alert('Error in Operation');
	    setBotResponse('error');
	  }
	}); 
}
  
  
	  //------------------------------------ Set bot response in result_div -------------------------------------
function setBotResponse(val) {
    setTimeout(function () {
	  var ResultsDiv = document.getElementById('result_div');
	  var mm = val.message;
	//  alert("mm "+mm);
  	  if (mm.trim() == '' || mm == 'error') { //if there is no response from bot or there is some error
    	  val = 'Sorry I wasn\'t able to understand your Query. Let\'s try something else!'
		  var BotResponse = '<p class="botResult">' + mm + '</p><div class="clearfix"></div>';
		  ResultsDiv.innerHTML=ResultsDiv.innerHTML+BotResponse;
	  } else {			
		 if(mm.includes(";")){//Split Message if it contains word -split
		  var valArray = mm.split(";")
		  for (i = 0; i < valArray.length; i++) {
			  var msg = "";
			  if (valArray[i].includes("{")){
				  eval('var obj='+valArray[i]);
				  addSuggestion(obj);
			  } else{
				  msg += '<p class="botResult">' + valArray[i] + '</p><div class="clearfix"></div>';
				  BotResponse = msg;
				  ResultsDiv.innerHTML=ResultsDiv.innerHTML+BotResponse;
			  }
	 	  }
		 } else{
			  var msg = "";
			  msg += '<p class="botResult">' + mm + '</p><div class="clearfix"></div>';
			  BotResponse = msg;
			  ResultsDiv.innerHTML=ResultsDiv.innerHTML+BotResponse;
		  }
		  s_id = val.s_id;
		  context = val.context;
		//  alert(s_id +" s_id context "+context)
		}
		scrollToBottomOfResults();
		hideSpinner();
	}, 500);
}

//------------------------------------------- Buttons(suggestions)--------------------------------------------------
function addSuggestion(textToAdd) {
	setTimeout(function () {
		var ResultsDiv = document.getElementById('result_div');
		var suggestions = textToAdd;
		var suggLength = textToAdd.length;
		ResultsDiv.innerHTML=ResultsDiv.innerHTML+'<p class="suggestion"></p>';
		// Loop through suggestions
		for (i = 0; i < suggLength; i++) {
			$('<span class="sugg-options">' + suggestions[i].title + '</span>').appendTo('.suggestion');
		}
		scrollToBottomOfResults();
	}, 1000);
}
  
	  //------------------------------------- Set user response in result_div ------------------------------------
function setUserResponse(val) {
	var UserResponse = '<p class="userEnteredText">' + val + '</p><div class="clearfix"></div>';
	var ResultsDiv = document.getElementById('result_div');
	//alert("setting user response "+UserResponse);
	ResultsDiv.innerHTML=ResultsDiv.innerHTML+UserResponse;
	document.getElementById("chat-input").value="";
	scrollToBottomOfResults();
	showSpinner();
   //$('.suggestion').remove();
}
  
  
	  //---------------------------------- Scroll to the bottom of the results div -------------------------------
function scrollToBottomOfResults() {
   var terminalResultsDiv = document.getElementById('result_div');
   terminalResultsDiv.scrollTop = terminalResultsDiv.scrollHeight;
}
  
  
	  //---------------------------------------- Spinner ---------------------------------------------------
function showSpinner() {               
  document.getElementById("spinnerID").style.display = "block";
}
  
function hideSpinner() {
  document.getElementById("spinnerID").style.display = "none";
}