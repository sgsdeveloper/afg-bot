const express = require('express')
const app = express();
const dialogflow = require('dialogflow');
const uuid = require('uuid');
app.use('/script', express.static('script'));

app.get('/', (req, res) => {
    res.sendFile( __dirname + "/" + "index.html" );
});

app.listen(8000, () => {
  console.log('Example app listening on port 8000!')
});

app.get('/logo.png', function (req, res) {
    res.sendFile( __dirname + "/image/" + "logo.PNG" );
}) 

app.get('/chatBot.js', function (req, res) {
    res.sendFile( __dirname + "/chatBot.JS" );
}) 

app.get('/dialogflow.js', function (req, res) {
    res.sendFile( __dirname + "/dialogflow.JS" );
}) 

app.get('/chatBot.css', function (req, res) {
    res.sendFile( __dirname + "/style/" + "chatbot.css" );
}) 

/*
//calling without error handling

app.get('/dflow', async function (req, res) {   
  console.log("returning  : "); 
  const result = await connectDialogflow("newagent-5ddad", req); 
  console.log("returning  : "+result);
  res.send(result);
}) 
*/


const awaitHandlerFactory = (middleware) => {
  return async (req, res,next) => {
    try {
      await middleware(req, res, next)
    } catch (err) {
      next(err)
    }
  }
}

app.get('/dflow', awaitHandlerFactory(async (req, res) => {
  const result = await connectDialogflow("newagent-5ddad", req);
  console.log('response from dialogflow  : {"message": "'+result+'","s_id": "'+req.query.s_id+'","context": "'+req.query.context+'"}');
  res.send('{"message": "'+result+'","s_id": "'+req.query.s_id+'","context": "'+req.query.context+'"}')
}))

  

/**
 * Send a query to the dialogflow agent, and return the query result.
 * @param {string} projectId The project to be used
 */

app.get('/session',function (req,res){ 
     sessionId = uuid.v4();
    res.send(sessionId);
});


async function connectDialogflow(projectId = 'newagent-5ddad', req) {
    console.log("Calling dialogflow ,Project id : "+projectId+" session id : "+req.query.s_id+
    " minput : "+req.query.minput+" context : "+req.query.context);
 
    // Create a new session
    const sessionClient = new dialogflow.SessionsClient();
    const sessionPath = sessionClient.sessionPath(projectId, req.query.s_id);
 
    // The text query request.
    const request = {
      session: sessionPath,
      queryInput: {
        text: {
          // The query to send to the dialogflow agent
          text: req.query.minput,
          // The language used by the client (en-US)
          languageCode: 'en-US',
        },
      },
    };
 
    // Send request and log result
    const responses = await sessionClient.detectIntent(request);
    console.log('Received response from dialogflow');
    const result = responses[0].queryResult;
    console.log(`Query : ${result.queryText}  Response: ${result.fulfillmentText}`);
    if (result.intent) {
      console.log(`Intent: ${result.intent.displayName}`);
    } else {
      console.log(`No intent matched.`);
    }
    return result.fulfillmentText;
 
}